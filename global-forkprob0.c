// fprintf stderr printf
#include <stdio.h>

// strerror
#include <string.h>

// error
#include <errno.h>

// exit
#include <stdlib.h>

// pid_t
#include <sys/types.h>

// fork
#include <unistd.h>

void unix_error (char* msg){
  fprintf (stderr, "%s: %s\n", msg, strerror (errno));
  exit (0);
}

pid_t Fork(void){
  pid_t pid;

  if ((pid = fork ()) < 0){
    unix_error("fork error");
  }
  return pid;
}

int main(int argc, char* argv){
  int a = 9;
  if(Fork () == 0){
    printf("p1: a=%d\n", a--);
  }
  printf("p2: a=%d\n", a++);
  exit(0);
}

/*
example run:

ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ gcc -o global-forkprob0 global-forkprob0.c
ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ ./global-forkprob0
p2: a=9
p1: a=9
p2: a=8
ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ 


parent prints:
p2: a=9

child prints:
p1: a=9
p2: a=8
*/
