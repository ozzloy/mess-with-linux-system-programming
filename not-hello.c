#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char* argv){
  char * who = "not-hello";
  printf("{%s\n", who);
  pid_t p = getpid();
  pid_t pp = getppid();
  printf("\t%s pid\t%ld\n", who, (long)p);
  printf("\t%s ppid\t%ld\n", who, (long)pp);
  printf("%s}\n", who);
  return 0;
}
