#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

void unix_error (char* msg){
  fprintf (stderr, "%s: %s\n", msg, strerror (errno));
  exit (0);
}

pid_t Fork(void){
  pid_t pid;

  if ((pid = fork ()) < 0){
    unix_error("fork error");
  }
  return pid;
}

int main (int argc, char* argv){
  write(1, "hello, world\n", 13);
  char *who;
  pid_t child_pid = Fork();
  if(child_pid){
    who = "parent";
  }
  else{
    who = "child ";
  }
  pid_t p = getpid();
  pid_t pp = getppid();
  printf("%s pid      \t%ld\n", who, (long)p);
  printf("%s ppid     \t%ld\n", who, (long)pp);
  printf("%s child_pid\t%ld\n", who, (long)child_pid);
  printf("%s exec'ing not-hello\n", who);
  execlp("./not-hello", "./not-hello", NULL);
  return 0;
}

/*
  example run:

ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ make
gcc -S hello.c
gcc -o hello hello.s
./hello
hello, world
parent pid      	495427
parent ppid     	495420
parent child_pid	495428
parent exec'ing not-hello
child  pid      	495428
child  ppid     	495427
child  child_pid	0
child  exec'ing not-hello
{not-hello
{not-hello
	not-hello pid	495427
	not-hello pid	495428
	not-hello ppid	495420
	not-hello ppid	495427
not-hello}
not-hello}
ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ make
./hello
hello, world
parent pid      	495534
parent ppid     	495533
parent child_pid	495535
parent exec'ing not-hello
child  pid      	495535
child  ppid     	495534
child  child_pid	0
child  exec'ing not-hello
{not-hello
{not-hello
	not-hello pid	495534
	not-hello pid	495535
	not-hello ppid	495533
	not-hello ppid	495534
not-hello}
not-hello}
ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ 
*/
