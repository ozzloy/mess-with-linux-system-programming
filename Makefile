.PHONY: run-waitpid1 run-waitprob1 run-hello clean waitpid2
CC=gcc
CFLAGS=-I.

all:run

waitprob1.o: waitprob1.c
	$(CC) -c -o $@ $<

csapp.o: csapp.c csapp.h
	$(CC) -c csapp.c

waitpid1.o:waitpid1.c csapp.h
	$(CC) -c -o $@ $<

waitpid2.o: waitpid2.c csapp.h
	$(CC) -c -o $@ $<

shellex.o: shellex.c csapp.h
	$(CC) -c -o $@ $<

myecho.o: myecho.c myecho.h
	$(CC) -c -o $@ $<

run:run-hello run-waitpid1 run-waitprob1

run-hello:hello
	./$<

run-waitprob1:waitprob1
	./$<

hello: hello.s not-hello
	$(CC) -o hello hello.s

hello.s: hello.c
	$(CC) -S $<

not-hello: not-hello.c
	$(CC) -o $@ $^

waitpid1:waitpid1.o csapp.o
	$(CC) -pthread -o $@ $^

run-waitpid1:waitpid1
	./$<

waitprob1: waitprob1.o csapp.o
	$(CC) -pthread -o waitprob1 waitprob1.o csapp.o

waitpid2: waitpid2.o csapp.o
	$(CC) -pthread -o waitpid2 waitpid2.o csapp.o

run-waitpid2:waitpid2
	./$<

run-myecho:myecho
	./$<

myecho: myecho.o
	$(CC) -o $@ $^

run-shellex: shellex
	./$<

shellex: shellex.o csapp.o
	$(CC) -pthread -o $@ shellex.o csapp.o

myshellex.o: myshellex.c csapp.h
	$(CC) -c -o $@ $<
clean:
	rm -f *.o

new:clean run
