#include "csapp.h"

int main(int argc, char* argv){
  int status;
  pid_t pid;

  printf("start\n");
  pid = Fork();
  printf("%d\n", !pid);
  if(pid == 0){
    printf("Child\n");
  }
  else if((waitpid(-1, &status, 0) > 0) && (WIFEXITED(status) != 0)){
    printf("%d\n", WEXITSTATUS(status));
  }
  printf("stop\n");
  exit(0);
}
