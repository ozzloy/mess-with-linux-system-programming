#include "myecho.h"

/* int main(int argc, char* argv){ */
/*   printf("hello, world!\n"); */
/* } */

int main(int argc, char*argv[], char*envp[]){
  for(int arg_index = 0; arg_index < argc; arg_index++){
    printf("argv[% 2d]: %s\n", arg_index, argv[arg_index]);
  }
  int env_index = 0;
  while(envp[env_index]){
    printf("envp[% 3d]: %s\n", env_index, envp[env_index]);
    env_index++;
  }
}



/*
ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ make run-myecho
gcc -c -o myecho.o myecho.c
gcc -o myecho myecho.o
./myecho
asdf
argv[ 0]: ./myecho
envp[ 0]: GNOME_SHELL_SESSION_MODE=pop
envp[ 1]: HOMEBREW_PREFIX=/home/linuxbrew/.linuxbrew
envp[ 2]: GJS_DEBUG_OUTPUT=stderr
envp[ 3]: LC_NAME=en_US.UTF-8
envp[ 4]: LC_NUMERIC=en_US.UTF-8
envp[ 5]: WINDOWPATH=2
envp[ 6]: CAML_LD_LIBRARY_PATH=/home/ozzloy/.opam/default/lib/stublibs:/usr/local/lib/ocaml/4.08.1/stublibs:/usr/lib/ocaml/stublibs
envp[ 7]: NVM_DIR=/home/ozzloy/.nvm
envp[ 8]: LC_ADDRESS=en_US.UTF-8
envp[ 9]: HISTFILE=/home/ozzloy/.bash_eternal_history
envp[ 10]: QT_ACCESSIBILITY=1
envp[ 11]: PWD=/home/ozzloy/tutoring/wyzant/ruth-e/my
envp[ 12]: XDG_DATA_DIRS=/usr/share/pop:/home/ozzloy/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share/:/usr/share/:/var/lib/snapd/desktop
envp[ 13]: HISTTIMEFORMAT=%F %T 
envp[ 14]: NVM_INC=/home/ozzloy/.nvm/versions/node/v10.16.0/include/node
envp[ 15]: LANG=en_US.UTF-8
envp[ 16]: XAUTHORITY=/run/user/1000/gdm/Xauthority
envp[ 17]: HOMEBREW_REPOSITORY=/home/linuxbrew/.linuxbrew/Homebrew
envp[ 18]: MAKEFLAGS=
envp[ 19]: LESSOPEN=| /usr/bin/lesspipe %s
envp[ 20]: MFLAGS=
envp[ 21]: SSH_AUTH_SOCK=/run/user/1000/keyring/ssh
envp[ 22]: NVM_BIN=/home/ozzloy/.nvm/versions/node/v10.16.0/bin
envp[ 23]: HISTSIZE=
envp[ 24]: XDG_CONFIG_DIRS=/etc/xdg/xdg-pop:/etc/xdg
envp[ 25]: XDG_SESSION_DESKTOP=pop
envp[ 26]: OCAML_TOPLEVEL_PATH=/home/ozzloy/.opam/default/lib/toplevel
envp[ 27]: XDG_SESSION_TYPE=x11
envp[ 28]: SESSION_MANAGER=local/poppy:@/tmp/.ICE-unix/1418,unix/poppy:/tmp/.ICE-unix/1418
envp[ 29]: MANPATH=/home/ozzloy/.nix-profile/share/man:/home/linuxbrew/.linuxbrew/share/man::/home/ozzloy/.nvm/versions/node/v10.16.0/share/man:/home/ozzloy/.opam/default/man
envp[ 30]: DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
envp[ 31]: HISTFILESIZE=
envp[ 32]: GNOME_DESKTOP_SESSION_ID=this-is-deprecated
envp[ 33]: INFOPATH=/home/linuxbrew/.linuxbrew/share/info:
envp[ 34]: SHELL=/bin/bash
envp[ 35]: XMODIFIERS=@im=ibus
envp[ 36]: GJS_DEBUG_TOPICS=JS ERROR;JS LOG
envp[ 37]: SHLVL=1
envp[ 38]: PATH=/home/ozzloy/.opam/default/bin:/home/ozzloy/.rbenv/shims:/home/ozzloy/.nvm/versions/node/v10.16.0/bin:/home/ozzloy/.nix-profile/bin:/home/ozzloy/.local/bin:/home/ozzloy/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/ozzloy/.rvm/bin
envp[ 39]: NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
envp[ 40]: LANGUAGE=en_US:en
envp[ 41]: LC_MONETARY=en_US.UTF-8
envp[ 42]: LC_TIME=en_US.UTF-8
envp[ 43]: INVOCATION_ID=84371ad528304748be7c2ec3d1043399
envp[ 44]: USERNAME=ozzloy
envp[ 45]: LESSCLOSE=/usr/bin/lesspipe %s %s
envp[ 46]: LC_TELEPHONE=en_US.UTF-8
envp[ 47]: GTK_IM_MODULE=ibus
envp[ 48]: XDG_CURRENT_DESKTOP=pop:GNOME
envp[ 49]: LS_COLORS=
envp[ 50]: DESKTOP_SESSION=pop
envp[ 51]: LOGNAME=ozzloy
envp[ 52]: GIO_LAUNCHED_DESKTOP_FILE=/usr/local/share/applications/emacs.desktop
envp[ 53]: DISPLAY=:0
envp[ 54]: GTK_MODULES=gail:atk-bridge:appmenu-gtk-module
envp[ 55]: USER=ozzloy
envp[ 56]: NIX_PROFILES=/nix/var/nix/profiles/default /home/ozzloy/.nix-profile
envp[ 57]: MANAGERPID=1118
envp[ 58]: OPAM_SWITCH_PREFIX=/home/ozzloy/.opam/default
envp[ 59]: HOMEBREW_CELLAR=/home/linuxbrew/.linuxbrew/Cellar
envp[ 60]: LC_MEASUREMENT=en_US.UTF-8
envp[ 61]: GIO_LAUNCHED_DESKTOP_FILE_PID=3242
envp[ 62]: SSH_AGENT_PID=1363
envp[ 63]: _=/usr/bin/make
envp[ 64]: LC_PAPER=en_US.UTF-8
envp[ 65]: MAKE_TERMOUT=/dev/pts/2
envp[ 66]: XDG_RUNTIME_DIR=/run/user/1000
envp[ 67]: GPG_AGENT_INFO=/run/user/1000/gnupg/S.gpg-agent:0:1
envp[ 68]: NVM_CD_FLAGS=
envp[ 69]: JOURNAL_STREAM=8:32526
envp[ 70]: MAKE_TERMERR=/dev/pts/2
envp[ 71]: XDG_SESSION_CLASS=user
envp[ 72]: INSIDE_EMACS=27.1.90,comint
envp[ 73]: HOME=/home/ozzloy
envp[ 74]: QT_IM_MODULE=ibus
envp[ 75]: TERM=dumb
envp[ 76]: PAPERSIZE=letter
envp[ 77]: NIX_PATH=/home/ozzloy/.nix-defexpr/channels
envp[ 78]: COLUMNS=97
envp[ 79]: RBENV_SHELL=bash
envp[ 80]: XDG_MENU_PREFIX=gnome-
envp[ 81]: TERMCAP=
envp[ 82]: GDMSESSION=pop
envp[ 83]: LC_IDENTIFICATION=en_US.UTF-8
envp[ 84]: MAKELEVEL=1
ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ ./myecho a s d f
asdf
argv[ 0]: ./myecho
argv[ 1]: a
argv[ 2]: s
argv[ 3]: d
argv[ 4]: f
envp[ 0]: SHELL=/bin/bash
envp[ 1]: SESSION_MANAGER=local/poppy:@/tmp/.ICE-unix/1418,unix/poppy:/tmp/.ICE-unix/1418
envp[ 2]: CAML_LD_LIBRARY_PATH=/home/ozzloy/.opam/default/lib/stublibs:/usr/local/lib/ocaml/4.08.1/stublibs:/usr/lib/ocaml/stublibs
envp[ 3]: QT_ACCESSIBILITY=1
envp[ 4]: OCAML_TOPLEVEL_PATH=/home/ozzloy/.opam/default/lib/toplevel
envp[ 5]: XDG_CONFIG_DIRS=/etc/xdg/xdg-pop:/etc/xdg
envp[ 6]: NVM_INC=/home/ozzloy/.nvm/versions/node/v10.16.0/include/node
envp[ 7]: XDG_MENU_PREFIX=gnome-
envp[ 8]: GNOME_DESKTOP_SESSION_ID=this-is-deprecated
envp[ 9]: GTK_IM_MODULE=ibus
envp[ 10]: HISTSIZE=
envp[ 11]: LANGUAGE=en_US:en
envp[ 12]: TERMCAP=
envp[ 13]: LC_ADDRESS=en_US.UTF-8
envp[ 14]: GNOME_SHELL_SESSION_MODE=pop
envp[ 15]: LC_NAME=en_US.UTF-8
envp[ 16]: SSH_AUTH_SOCK=/run/user/1000/keyring/ssh
envp[ 17]: INSIDE_EMACS=27.1.90,comint
envp[ 18]: HISTTIMEFORMAT=%F %T 
envp[ 19]: HOMEBREW_PREFIX=/home/linuxbrew/.linuxbrew
envp[ 20]: XMODIFIERS=@im=ibus
envp[ 21]: DESKTOP_SESSION=pop
envp[ 22]: LC_MONETARY=en_US.UTF-8
envp[ 23]: RBENV_SHELL=bash
envp[ 24]: SSH_AGENT_PID=1363
envp[ 25]: GTK_MODULES=gail:atk-bridge:appmenu-gtk-module
envp[ 26]: PWD=/home/ozzloy/tutoring/wyzant/ruth-e/my
envp[ 27]: NIX_PROFILES=/nix/var/nix/profiles/default /home/ozzloy/.nix-profile
envp[ 28]: LOGNAME=ozzloy
envp[ 29]: XDG_SESSION_DESKTOP=pop
envp[ 30]: XDG_SESSION_TYPE=x11
envp[ 31]: MANPATH=/home/ozzloy/.nix-profile/share/man:/home/linuxbrew/.linuxbrew/share/man::/home/ozzloy/.nvm/versions/node/v10.16.0/share/man:/home/ozzloy/.opam/default/man
envp[ 32]: NIX_PATH=/home/ozzloy/.nix-defexpr/channels
envp[ 33]: GPG_AGENT_INFO=/run/user/1000/gnupg/S.gpg-agent:0:1
envp[ 34]: XAUTHORITY=/run/user/1000/gdm/Xauthority
envp[ 35]: OPAM_SWITCH_PREFIX=/home/ozzloy/.opam/default
envp[ 36]: GJS_DEBUG_TOPICS=JS ERROR;JS LOG
envp[ 37]: WINDOWPATH=2
envp[ 38]: HOME=/home/ozzloy
envp[ 39]: USERNAME=ozzloy
envp[ 40]: LANG=en_US.UTF-8
envp[ 41]: LC_PAPER=en_US.UTF-8
envp[ 42]: HISTFILE=/home/ozzloy/.bash_eternal_history
envp[ 43]: LS_COLORS=
envp[ 44]: XDG_CURRENT_DESKTOP=pop:GNOME
envp[ 45]: COLUMNS=97
envp[ 46]: NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
envp[ 47]: INVOCATION_ID=84371ad528304748be7c2ec3d1043399
envp[ 48]: MANAGERPID=1118
envp[ 49]: INFOPATH=/home/linuxbrew/.linuxbrew/share/info:
envp[ 50]: GJS_DEBUG_OUTPUT=stderr
envp[ 51]: NVM_DIR=/home/ozzloy/.nvm
envp[ 52]: LESSCLOSE=/usr/bin/lesspipe %s %s
envp[ 53]: XDG_SESSION_CLASS=user
envp[ 54]: LC_IDENTIFICATION=en_US.UTF-8
envp[ 55]: TERM=dumb
envp[ 56]: LESSOPEN=| /usr/bin/lesspipe %s
envp[ 57]: USER=ozzloy
envp[ 58]: HOMEBREW_CELLAR=/home/linuxbrew/.linuxbrew/Cellar
envp[ 59]: DISPLAY=:0
envp[ 60]: SHLVL=1
envp[ 61]: NVM_CD_FLAGS=
envp[ 62]: LC_TELEPHONE=en_US.UTF-8
envp[ 63]: QT_IM_MODULE=ibus
envp[ 64]: HOMEBREW_REPOSITORY=/home/linuxbrew/.linuxbrew/Homebrew
envp[ 65]: LC_MEASUREMENT=en_US.UTF-8
envp[ 66]: PAPERSIZE=letter
envp[ 67]: XDG_RUNTIME_DIR=/run/user/1000
envp[ 68]: LC_TIME=en_US.UTF-8
envp[ 69]: JOURNAL_STREAM=8:32526
envp[ 70]: XDG_DATA_DIRS=/usr/share/pop:/home/ozzloy/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share/:/usr/share/:/var/lib/snapd/desktop
envp[ 71]: PATH=/home/ozzloy/.opam/default/bin:/home/ozzloy/.rbenv/shims:/home/ozzloy/.nvm/versions/node/v10.16.0/bin:/home/ozzloy/.nix-profile/bin:/home/ozzloy/.local/bin:/home/ozzloy/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/ozzloy/.rvm/bin
envp[ 72]: GDMSESSION=pop
envp[ 73]: HISTFILESIZE=
envp[ 74]: DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
envp[ 75]: NVM_BIN=/home/ozzloy/.nvm/versions/node/v10.16.0/bin
envp[ 76]: GIO_LAUNCHED_DESKTOP_FILE_PID=3242
envp[ 77]: GIO_LAUNCHED_DESKTOP_FILE=/usr/local/share/applications/emacs.desktop
envp[ 78]: LC_NUMERIC=en_US.UTF-8
envp[ 79]: _=./myecho
ozzloy@poppy:~/tutoring/wyzant/ruth-e/my$ 
*/
